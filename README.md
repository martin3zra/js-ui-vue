# S360 Tech Test - Vue (js-ui-vue)
Hello! Thank you for taking the time to complete this test. Below, you will find instructions on how to complete the UI
portion of the tech test. Please follow the setup instructions to get the UI up and running. If you run into issues
getting the UI to work, please reach out to Eric Schmiedel [eschmiedel@storage360.com](mailto:eschmiedel@storage360.com)
for further assistance.

## Goals
The goal of this portion of the test is to demonstrate your knowledge of working with JavaScript/TypeScript, Vue3,
Pinia, Axios, and a UI component library. It is also meant to test your ability to quickly learn the conventions of a
new codebase, follow those conventions for the sake of consistency, and test your ability to debug.

## Setup Instructions
This setup assumes that you have a development environment that has Node.js and yarn installed, a terminal, and
knowledge on how to use the terminal. **It also assumes you have already set up the PHP-powered Laravel API.**

1. Fork this repository so that you have your own version in your GitLab account
   * Make sure that you mark your repository as public so that we can review it
2. Open your terminal and clone your fork (**DO NOT CLONE THE MAIN REPOSITORY!**)
3. `$ yarn`
4. `$ yarn quasar dev`
5. The site should open in a new browser tab

## UI Description & Tasks
This is a fairly simple Vue3 app that is built using Quasar. Prior knowledge of Quasar is helpful but not required. If
you have never used a component library such as Quasar, Vuetify, Bootstrap, etc. then it is recommended you take a look
at the [Quasar documentation](https://quasar.dev) for some examples.

### Task 1
Change the router mode so that the URL no longer contains the `/#/`.

### Task 2
The product owner wants to change the 'Cat Facts' section based on feedback from users. Please read the user story
below and refactor the 'Cats' section as necessary to meet the **Acceptance Criteria**. Also, please remove any files
that are no longer necessary for the application to function.

#### User Story
As a user, I should be able to see the 'Cats' section laid out in a card format with infinite scroll so that I can
easily consume 'Cat Facts'. I should also be able to add, edit, and delete cat facts without navigating away from the
page.

**Acceptance Criteria**
* The table is removed from the page
* I should be able to see the 'Cat Facts' laid out in a card deck format
  * Desktop/Landscape tablet: 6 cards per row
  * Portrait tablet: 3 cards per row
  * Phone: 1 card per row
  * The card should contain the fact, an edit icon, and a delete icon
* I should see a pencil icon on the card that, when clicked, allows me to edit the fact
  * I should NOT be navigated away from the current page
  * I should see a modal appear prompting me to input the fact with a text input field
  * I should be able to click a 'Save Fact' button in the modal that saves the fact
  * I should be able to click a 'Cancel' button in the modal that clears the input box and hides the modal
* I should see a trash can icon on the cat, when clicked, allows me to delete the fact
* I should be able to infinitely scroll and see more cat fact cards appear dynamically when I reach the bottom
* I should be able to click on the '+ Add New Fact' button to add a new fact
  * I should NOT be navigated away from the current page
  * I should see a modal appear prompting me to input the fact with a text input field
  * I should be able to click a 'Save Fact' button in the modal that saves the fact
  * I should be able to click a 'Cancel' button in the modal that clears the input box and hides the modal

### Bonus Task
Implement a section that consumes the 'dog-facts' endpoints you created in the PHP API task.

## Submission
After you have completed the tasks, please commit your local files and **push them up to your fork**. Notify the person
you have been working with that you have completed the test and provide them a link to **your** fork.
